#ifndef IAUTOMATON_H
#define IAUTOMATON_H

#include <QString>
#include <QColor>

#include <vector>

struct StateProperty
{
    StateProperty() {}
    StateProperty( const QString& name, int value ): m_name( name ), m_value( value ) {}

public:
    QString m_name;
    int     m_value;
};

struct State: public std::vector<StateProperty>
{
    QString m_name;
};

// абстрактный клеточный автомат
class IAutomaton
{
public:
    typedef std::list<State>    StatesList;

public:
    virtual ~IAutomaton() {}

    // инициализация - заполняет автомат полями по умолчанию
    virtual void init_clear( int width, int height ) = 0;

    // параметры автомата
    virtual QString name() const = 0;
    virtual int width() const = 0;
    virtual int height() const = 0;

    // состояние по умолчанию
    virtual StatesList getDefaultStates() const = 0;

    // взаимодействие
    virtual QColor cellColor( int row, int col ) const = 0;
    virtual State cell( int row, int col ) const = 0;

    virtual void setCell( int row, int col, const State& state ) = 0;

    // вычисляет следующее состояние автомата
    virtual void next() = 0;

    virtual void store( const QString& name ) = 0;
    virtual void restore( const QString& name ) = 0;
};

#endif // IAUTOMATON_H

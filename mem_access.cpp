#include "mem_access.h"

#include <stdio.h>

//	packer
//////////////////////////////////////////////////////////////////////////
Packer::Packer(): m_pBuffer( NULL ), m_isSelfBuffer( false )
{
}

Packer::Packer( Buffer& buffer ): m_isSelfBuffer( false )
{
	attach( buffer );
}

Packer::Packer( EType type )
{
	if ( type == own_buffer )
	{
		m_isSelfBuffer = true;

		attach( *( new DynBuffer() ) );
	}
}

Packer::~Packer()
{
	if ( m_isSelfBuffer )
	{
		delete m_pBuffer; m_pBuffer = 0;
	}
}

void Packer::cometoPos( unsigned pos )
{
	m_pos = pos;
}

void Packer::push( const void* pData, unsigned bytes_count )
{
	unsigned sz = m_pBuffer->size();

	if ( m_pos + bytes_count > sz )
	{
		m_pBuffer->resize( sz + bytes_count );
	}

	memcpy( &m_pBuffer->u8( m_pos ), (const unsigned char*)pData, bytes_count );

	m_pos += bytes_count;
}

void Packer::push( const Buffer& buffer )
{
	push( buffer.data(), buffer.size() );
}

const Buffer& Packer::buffer() const
{
	return *m_pBuffer;
}

Buffer& Packer::buffer()
{
	return *m_pBuffer;
}

void Packer::attach( Buffer& buffer )
{
	m_pBuffer = &buffer;
	m_pos = 0;
}


// Extractor
//////////////////////////////////////////////////////////////////////////
Extractor::Extractor( Buffer& buffer )
{
	attach( buffer );
}

Buffer& Extractor::buffer()
{
	return *m_pBuffer;
}

const Buffer& Extractor::buffer() const
{
	return *m_pBuffer;
}

void Extractor::attach( Buffer& buffer )
{
	m_pBuffer = &buffer;

	cometoPos( 0 );
}

uchar* Extractor::pointer()
{
	return m_pPointer;
}

void Extractor::cometoPos( unsigned pos )
{
	m_pPointer = m_pBuffer->data() + pos;
}

void Extractor::incrementPointer( unsigned delta )
{
	m_pPointer += delta;
}

void Extractor::get( uchar* pAddr, unsigned size )
{
	// check for overflow
	if ( m_pPointer + size > m_pBuffer->data() + m_pBuffer->size() )
	{
		//	EXCEXC
	}

	memcpy( pAddr, m_pPointer, size );
	m_pPointer += size;
}

void Packer::clear()
{
	m_pBuffer->resize( 0 );
	m_pos = 0;
}

const uchar* Packer::data() const
{
	return m_pBuffer->data();
}

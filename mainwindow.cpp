#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "life.h"

#include <QPaintEvent>
#include <QDebug>
#include <QInputDialog>
#include <QString>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow), m_pAutomaton( 0 )
{
    ui->setupUi(this);

    m_pArea = new TestingArea( ui->frame );
    m_pArea->setScrolls( ui->horScroll, ui->vertScroll );
    ui->gridLayout->addWidget(m_pArea, 0, 0, 1, 1);

    m_isEditing = false;

    configView();
    changeAutomaton( 0, 50, 50 );
}

MainWindow::~MainWindow()
{
    delete m_pArea; m_pArea = 0;

    delete ui;
}

void MainWindow::configView()
{
    ui->btnPlay->setIcon( style()->standardIcon( QStyle::SP_MediaPlay ) );
    ui->btnPlay->setText( "" );

    ui->btnStop->setIcon( style()->standardIcon( QStyle::SP_MediaStop ) );
    ui->btnStop->setText( "" );

    ui->btnLoad->setIcon( style()->standardIcon( QStyle::SP_DirOpenIcon ) );
    ui->btnLoad->setText( "" );

    ui->btnSave->setIcon( style()->standardIcon( QStyle::SP_TrashIcon ) );
    ui->btnSave->setText( "" );

    ui->btnResetAuto->setIcon( style()->standardIcon( QStyle::SP_DialogApplyButton ) );


    ui->comboAutomatons->addItem( "life" );

    connect( ui->attribEdit, SIGNAL( itemDoubleClicked( QTreeWidgetItem*, int ) ), SLOT( attribDoubleClicked(QTreeWidgetItem*,int) ) );
    connect( ui->btnEditAutomaton, SIGNAL( toggled(bool) ), SLOT( onBtnEdit(bool) ) );
    connect( ui->btnPlay, SIGNAL( clicked() ), SLOT( onPlay()) );
    connect( ui->btnStop, SIGNAL( clicked() ), SLOT( onStop()) );
    connect( &m_timer, SIGNAL( timeout() ), SLOT( onTime() ) );
    connect( ui->btnResetAuto, SIGNAL( clicked() ), SLOT( onResetAutomaton()) );
    connect( ui->btnSave, SIGNAL( clicked() ), SLOT( saveAutomaton()) );
    connect( ui->btnLoad, SIGNAL( clicked() ), SLOT( openAutomaton()) );
    connect( ui->listStates, SIGNAL( currentRowChanged( int ) ), SLOT( selectOtherState( int ) ) );

    ui->btnStop->setEnabled( false );
}

void MainWindow::changeAutomaton( int index, int width, int height )
{
    delete m_pAutomaton; m_pAutomaton = 0;

    switch ( index )
    {
        case 0: m_pAutomaton = new life::Automaton(); break;
    }

    m_pAutomaton->init_clear( width, height );

    m_pArea->attach( m_pAutomaton );
    m_pArea->init();

    ui->attribEdit->clear();
    ui->listStates->clear();

    IAutomaton::StatesList def = m_pAutomaton->getDefaultStates();

    IAutomaton::StatesList::const_iterator lit = def.begin();
    for (; lit != def.end(); ++lit )
    {
        const State& st = *lit;

        ui->listStates->addItem( st.m_name );
    }

    ui->listStates->setCurrentRow( 0 );

    m_pArea->update();
}

void MainWindow::attribDoubleClicked( QTreeWidgetItem* item, int column )
{
    bool ok;
    int res = QInputDialog::getInt( this, QString::fromUtf8( "Введите значение" ), item->text( 0 ), 0, -2147483647, 2147483647, 1, &ok );

    if ( ok )
    {
        item->setText( 1, QString::number( res ) );

        if ( ui->btnEditAutomaton->isChecked() )
        {
            onBtnEdit( true );
        }
    }
}

void MainWindow::onBtnEdit( bool is_on )
{
    m_isEditing = is_on;

    if ( !is_on )
    {
        // выключаем режим редактирования
        m_pArea->setModeEditing( State(), false );
        return;
    }

    State st = m_pAutomaton->getDefaultStates().front();

    State::iterator it = st.begin();
    for ( int i = 0; it != st.end(); ++i, ++it )
    {
        StateProperty& prop = *it;

        prop.m_value = ui->attribEdit->topLevelItem( i )->text( 1 ).toInt();
    }

    m_pArea->setModeEditing( st, true );
}

void MainWindow::onPlay()
{
    m_timer.start( 100 );

    ui->btnPlay->setEnabled( false );
    ui->btnStop->setEnabled( true );
}

void MainWindow::onStop()
{
    m_timer.stop();

    ui->btnPlay->setEnabled( true );
    ui->btnStop->setEnabled( false );
}

void MainWindow::onTime()
{
    m_pAutomaton->next();

    m_pArea->update();
}

void MainWindow::onResetAutomaton()
{
    bool ok;
    int width = ui->edtWidth->text().toInt( &ok );
    if ( !ok ) return;

    int height = ui->edtHeight->text().toInt( &ok );
    if ( !ok ) return;

    changeAutomaton( ui->comboAutomatons->currentIndex(), width, height );
}

void MainWindow::openAutomaton()
{
    QString fileName = QFileDialog::getOpenFileName( this, "open automaton", "", "automaton files (*.aur);; all files (*)" );
    if ( fileName == "" )
    {
        return;
    }

    m_pAutomaton->restore( fileName );

    m_pArea->update();
}

void MainWindow::saveAutomaton()
{
    QString fileName = QFileDialog::getSaveFileName( this, "save automaton", "", "automaton files (*.aur);; all files (*)" );
    if ( fileName == "" )
    {
        return;
    }

    m_pAutomaton->store( fileName + ".aur");
}

void MainWindow::selectOtherState( int index )
{
    ui->attribEdit->clear();

    IAutomaton::StatesList def = m_pAutomaton->getDefaultStates();

    IAutomaton::StatesList::const_iterator lit = def.begin();
    for (; lit != def.end() && index > 0; )
    {
        index--; ++lit;
    }

    State::const_iterator it = lit->begin();
    for (; it != lit->end(); ++it )
    {
        const StateProperty& att = *it;

        QStringList representation;
        representation.push_back( att.m_name );
        representation.push_back( QString::number( att.m_value ) );

        new QTreeWidgetItem( ui->attribEdit, representation );
    }

    if ( m_isEditing )
    {
        State st = m_pAutomaton->getDefaultStates().front();

        State::iterator it = st.begin();
        for ( int i = 0; it != st.end(); ++i, ++it )
        {
            StateProperty& prop = *it;

            prop.m_value = ui->attribEdit->topLevelItem( i )->text( 1 ).toInt();
        }

        m_pArea->setModeEditing( st, true );
    }
}












































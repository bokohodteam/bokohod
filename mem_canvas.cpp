#include "mem_canvas.h"

#include <iostream>

uchar* MemWrapper::data()
{
    return m_pBegin;
}

const uchar* MemWrapper::data() const
{
    return m_pBegin;
}

unsigned MemWrapper::size() const
{
    return m_sizeBytes;
}

void MemWrapper::resize( unsigned )
{
    std::cout << "try to resize mem_wrapper\n";
}

uchar* MemDynamic::data()
{
    return &m_buffer.front();
}

const uchar* MemDynamic::data() const
{
    return &m_buffer.front();
}

unsigned MemDynamic::size() const
{
    return m_buffer.size();
}

void MemDynamic::resize( unsigned new_bytes_count )
{
    m_buffer.resize( new_bytes_count );
}

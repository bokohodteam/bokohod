#include <QtGui>

#include "testingarea.h"
#include "iautomaton.h"

#include <iostream>

const int TestingArea::m_sMargin = 5;
const int TestingArea::m_sMinForLine = 10;

TestingArea::TestingArea( QWidget* parent ): QWidget( parent )
{
    m_pHorScroll = m_pVertScroll = 0;

    m_pAutomaton = 0;

    m_background = QBrush( Qt::black );

    m_scale = 1;

    setMouseTracking( true );

    m_highlightedCol = m_highlightedRow = -1;
    m_mouseMode = viewing;

    m_isEditing = false;
}

void TestingArea::setScrolls( QScrollBar* pHor, QScrollBar* pVert )
{
    m_pHorScroll = pHor;
    m_pVertScroll = pVert;

    connect( m_pHorScroll, SIGNAL( valueChanged( int ) ), SLOT( onHorScroll(int) ) );
    connect( m_pVertScroll, SIGNAL( valueChanged( int ) ), SLOT( onVertScroll(int) ) );
}

void TestingArea::paintEvent(QPaintEvent *)
{
    QRect rect = this->rect();

    QPainter painter;

    QPixmap pixmap( rect.width(), rect.height() );

    // вначале рисуем на внеэкранном пиксмапе
    painter.begin( &pixmap );

    // заливаем черным
    painter.fillRect( rect, m_background );

    // рисуем автомат
    draw( painter );

    painter.end();

    // отображаем пиксмап с нашим автоматом на экран
    painter.begin( this );
    painter.drawPixmap( rect, pixmap );
    painter.end();
}

void TestingArea::attach( IAutomaton* p )
{
    m_pAutomaton = p;
}

void TestingArea::init()
{
    if ( !m_pAutomaton || !m_pHorScroll || !m_pVertScroll )
    {
        return;
    }

    changeMouseMode( viewing );

    // вычисляем сторону ячейки
    m_screenWidth = this->rect().width();// - 2*m_sMargin;
    m_screenHeight = this->rect().height();// - 2*m_sMargin;

    int rows = m_pAutomaton->height();
    int cols = m_pAutomaton->width();

    double pix_per_cell_width = ( m_screenWidth - 2*m_sMargin )/double( cols );
    double pix_per_cell_height = ( m_screenHeight - 2*m_sMargin )/double( rows );

    m_baseSide = pix_per_cell_height < pix_per_cell_width ? int( pix_per_cell_height ) : int( pix_per_cell_width );
    if ( m_baseSide > 50 )
    {
        m_baseSide = 50;
    }

    if ( m_baseSide < 1 )
    {
        m_baseSide = 1;
    }

    m_side = m_baseSide*m_scale;

    int width = 2*m_sMargin + m_side*cols;
    int height = 2*m_sMargin + m_side*rows;

    m_horShift = 0;
    m_vertShift = 0;

    m_leftMargin = 0;
    m_topMargin = 0;

    m_pHorScroll->setRange( 0, width - m_screenWidth );
    m_pHorScroll->setPageStep( m_screenWidth );
    m_pHorScroll->setSliderPosition( m_horShift );

    m_pVertScroll->setRange( 0, height - m_screenHeight );
    m_pVertScroll->setPageStep( m_screenHeight );
    m_pVertScroll->setSliderPosition( m_vertShift );

    if ( m_screenWidth - 2*m_sMargin > cols*m_side )
    {
        m_leftMargin = ( m_screenWidth - 2*m_sMargin - cols*m_side )/2;
    }

    if ( m_screenHeight - 2*m_sMargin > rows*m_side )
    {
        m_topMargin = ( m_screenHeight - 2*m_sMargin - rows*m_side )/2;
    }
}

void TestingArea::draw( QPainter& painter )
{
    if ( !m_pAutomaton )
    {
        return;
    }

    draw_grid( painter );
    draw_automaton( painter );
}

QRect TestingArea::cell_rect( int row, int col ) const
{
    int x1 = m_sMargin + m_side*col - m_horShift + m_leftMargin;
    int y1 = m_sMargin + m_side*row - m_vertShift + m_topMargin;

    return QRect( x1, y1, m_side, m_side );
}

void TestingArea::resizeEvent( QResizeEvent* event )
{
    init();
}

void TestingArea::draw_grid( QPainter& painter )
{
    int rows = m_pAutomaton->height();
    int cols = m_pAutomaton->width();

    QRect lastrect = cell_rect( rows - 1, cols - 1 );

    if ( lastrect.right() > m_screenWidth - m_sMargin )
    {
        lastrect.setRight( m_screenWidth - m_sMargin );
    }

    if ( lastrect.bottom() > m_screenHeight - m_sMargin )
    {
        lastrect.setBottom( m_screenHeight - m_sMargin );
    }

    // bounding quadrangle
    painter.setPen( QColor( 255, 255, 255 ) );
//    painter.drawLine( m_sMargin, m_sMargin, lastrect.right(), m_sMargin );
//    painter.drawLine( m_sMargin, m_sMargin, m_sMargin, lastrect.bottom() );
//    painter.drawLine( lastrect.right(), m_sMargin, lastrect.right(), lastrect.bottom() );
//    painter.drawLine( m_sMargin, lastrect.bottom(), lastrect.right(), lastrect.bottom() );

//    if ( m_side > m_sMinForLine )
//    {
//        // vert lines
//        for ( int i = 0; i < rows; ++i )
//        {
//            int x = m_sMargin + ( i + 1 )*m_side - m_horShift + m_leftMargin;

//            if ( x > m_sMargin && x < m_screenWidth - m_sMargin - m_leftMargin )
//            {
//                painter.drawLine( x, m_sMargin + m_topMargin, x, lastrect.bottom() );
//            }
//        }

//        // hor lines
//        for ( int i = 0; i < cols; ++i )
//        {
//            int y = m_sMargin + ( i + 1 )*m_side - m_vertShift + m_topMargin;

//            if ( y > m_sMargin && y < m_screenHeight - m_sMargin - m_topMargin )
//            {
//                painter.drawLine( m_sMargin + m_leftMargin, y, lastrect.right(), y );
//            }
//        }
//    }
}

void TestingArea::wheelEvent( QWheelEvent* event )
{
    if ( event->delta() > 0 && ( m_baseSide*m_scale*2 < 100 ) )
    {
        m_scale *= 2;
    }

    if ( event->delta() < 0 && m_scale > 1 )
    {
        m_scale /= 2;
    }

    init();

    // при приближении стараемся центрировать подсвеченную ячейку
    if ( m_highlightedCol != -1 && m_highlightedRow != -1 )
    {
        // вычисляем желаемый сдвиг
        int wanted_hor_shift = m_highlightedCol*m_side + m_side/2 - m_screenWidth/2 - m_sMargin - m_leftMargin;
        int wanted_vert_shift = m_highlightedRow*m_side + m_side/2 - m_screenHeight/2 - m_sMargin - m_topMargin;

        m_pHorScroll->setValue( wanted_hor_shift );
        m_pVertScroll->setValue( wanted_vert_shift );

        // координаты центра квадрата
        int x = m_highlightedCol*m_side + m_side/2 - m_horShift + m_sMargin + m_leftMargin;
        int y = m_highlightedRow*m_side + m_side/2 - m_vertShift + m_sMargin + m_topMargin;

        QCursor::setPos( mapToGlobal( QPoint( x, y ) ) );
    }

//    processMouseMoving();

    update();
}

void TestingArea::onHorScroll( int pos )
{
    m_horShift = pos;

    update();
}

void TestingArea::onVertScroll( int pos )
{
    m_vertShift = pos;

    update();
}

void TestingArea::processMouseMoving( QMouseEvent* event )
{
    const QPoint& pos = m_lastMousePoint;

    int highlightCol = ( pos.x() - m_sMargin + m_horShift - m_leftMargin )/m_side;
    int highlightRow = ( pos.y() - m_sMargin + m_vertShift - m_topMargin )/m_side;

    if ( pos.x() < m_sMargin || pos.x() > m_screenWidth - m_sMargin || pos.y() < m_sMargin || pos.y() > m_screenHeight - m_sMargin )
    {
        highlightRow = highlightCol = -1;
    }

    if ( highlightRow != m_highlightedRow || highlightCol != m_highlightedCol )
    {
        m_highlightedCol = highlightCol;
        m_highlightedRow = highlightRow;

        bool need_update = false;
        if ( m_isEditing && event->buttons() & Qt::LeftButton )
        {
            m_pAutomaton->setCell( m_highlightedRow, m_highlightedCol, m_stateToInsert );
            need_update = true;
        }

        if ( m_side > m_sMinForLine )
        {
            need_update = true;
        }

        if ( need_update )
        {
            update();
        }
    }
}

void TestingArea::mouseMoveEvent( QMouseEvent* pEvent )
{
    m_lastMousePoint = pEvent->pos();

    if ( m_mouseMode == viewing )
    {
        processMouseMoving( pEvent );
    }

    if ( m_mouseMode == rbutton_drag )
    {
        dragScreen();
    }
}

void TestingArea::dragScreen()
{
    int delta_x = m_pressPlace.x() - m_lastMousePoint.x();
    int delta_y = m_pressPlace.y() - m_lastMousePoint.y();

    m_pHorScroll->setValue( m_horShiftPlace + delta_x );
    m_pVertScroll->setValue( m_vertShiftPlace + delta_y );
}

void TestingArea::draw_automaton( QPainter& painter )
{
    // вычисляем начальные и конечные координаты ячеек, которые помещаются в экран

    int start_x = m_horShift/m_side;
    int stop_x = (m_horShift + m_screenWidth)/m_side + 1;

    int start_y = m_vertShift/m_side;
    int stop_y = (m_vertShift + m_screenHeight)/m_side + 1;

    if ( stop_x > m_pAutomaton->width() ) stop_x = m_pAutomaton->width();
    if ( stop_y > m_pAutomaton->height() ) stop_y = m_pAutomaton->height();

    for ( int i = start_y; i < stop_y; ++i )
    {
        for ( int j = start_x; j < stop_x; ++j )
        {
            QRect rect = cell_rect( i, j );

            if ( rect.right() <= m_sMargin || rect.left() > m_screenWidth - m_sMargin )
            {
                continue;
            }

            if ( rect.bottom() <= m_sMargin || rect.top() > m_screenHeight - m_sMargin )
            {
                continue;
            }

            if ( rect.top() < m_sMargin ) rect.setTop( m_sMargin );
            if ( rect.left() < m_sMargin ) rect.setLeft( m_sMargin );
            if ( rect.right() > m_screenWidth - m_sMargin ) rect.setRight( m_screenWidth - m_sMargin );
            if ( rect.bottom() > m_screenHeight - m_sMargin ) rect.setBottom( m_screenHeight - m_sMargin );

            QColor color = m_pAutomaton->cellColor( i, j );

            if ( i == m_highlightedRow && j == m_highlightedCol && m_side > m_sMinForLine )
            {
                color = color.lighter( 150 );
            }

            QBrush br;
            br.setColor( color );
            br.setStyle( Qt::SolidPattern );

            QPen pen;
            if ( m_side > m_sMinForLine )
            {
                pen.setColor( QColor( 200, 200, 200 ) );
            }
            else
            {
                pen.setColor( color );
            }

            painter.setBrush( br );
            painter.setPen( pen );

            painter.drawRect( rect );
        }
    }
}

void TestingArea::mousePressEvent( QMouseEvent* event )
{
    if ( event->button() == Qt::RightButton )
    {
        changeMouseMode( rbutton_drag );

        m_pressPlace = event->pos();

        m_horShiftPlace = m_horShift;
        m_vertShiftPlace = m_vertShift;
    }

    if ( event->button() == Qt::LeftButton )
    {
        if ( m_isEditing && m_highlightedCol > 0 && m_highlightedRow > 0 )
        {
            m_pAutomaton->setCell( m_highlightedRow, m_highlightedCol, m_stateToInsert );

            update();
        }
    }
}

void TestingArea::mouseReleaseEvent( QMouseEvent* event )
{
    changeMouseMode( viewing );
}

void TestingArea::changeMouseMode( EMouseMode mode )
{
    m_mouseMode = mode;

    if ( mode == viewing )
    {
        setCursor( QCursor( Qt::ArrowCursor ) );
    }

    if ( mode == rbutton_drag )
    {
        setCursor( QCursor( Qt::ClosedHandCursor ) );
    }
}

void TestingArea::setModeEditing( const State& state, bool is_editing )
{
    m_stateToInsert = state;
    m_isEditing = is_editing;
}


















































# PSYCHOHOD PROJECT DESCRIPTION #

![Психоход.png](https://bitbucket.org/repo/XpKrkM/images/200693783-%D0%9F%D1%81%D0%B8%D1%85%D0%BE%D1%85%D0%BE%D0%B4.png)

This project is devoted to developing the free agent-based simulation environment "Psychohod" (the name means something like "The one going psychically" in Russian). The language of the project is C++14. Lua 5.2 and Qt4 libraries are required for the compilation.

The project includes

* Graphical subsystem

* Computational core (a cellular automaton-based agent model)

* Scripting language interpreter

© All rights reserved, 2017. This program is the registered intellectual property of its developers, number 2017619605 in the Rospatent Register "Computer Programs".
### Contacts
[Dr. Alexander Kuznetsov](https://www.researchgate.net/profile/Alexandr_Kuznetsov3), Voronezh State University, AMM Faculty, associate professor.
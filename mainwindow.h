#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTreeWidgetItem>
#include <QTimer>

#include "testingarea.h"

class IAutomaton;

namespace Ui
{
    class MainWindow;
}

class MainWindow: public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    void configView();
    void changeAutomaton( int index, int width, int height );

private slots:
    void attribDoubleClicked( QTreeWidgetItem* item, int column );
    void onBtnEdit( bool is_on );
    void onPlay();
    void onStop();
    void onTime();
    void onResetAutomaton();
    void openAutomaton();
    void saveAutomaton();
    void selectOtherState( int index );

private:
    Ui::MainWindow *ui;

    TestingArea*    m_pArea;
    IAutomaton*     m_pAutomaton;
    QTimer          m_timer;
    bool            m_isEditing;
};

#endif // MAINWINDOW_H

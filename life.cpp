#include "life.h"
#include "mem_access.h"

#include <QMessageBox>
// trulala

namespace life
{

Automaton::Automaton()
{
}

// access
int Automaton::width() const
{
    return m_width;
}

int Automaton::height() const
{
    return m_height;
}

QColor Automaton::cellColor( int row, int col ) const
{
    return m_matrix[row][col].toColor();
}

State Automaton::cell( int row, int col ) const
{
    return State();
}

// initing
void Automaton::init_clear( int width, int height )
{
    m_matrix.clear();
    m_matrix.resize( height, StateLine( width ) );

    m_width = width;
    m_height = height;

    State state = getDefaultStates().front();
    for ( int i = 0; i < m_height; ++i )
    {
        for ( int j = 0; j < m_width; ++j )
        {
            setCell( i, j, state );
        }
    }

    m_default = m_matrix;
}

void Automaton::setCell( int row, int col, const State& state )
{
    Cell c;
    c.m_busy = state[0].m_value;

    m_matrix[row][col] = c;
}

QColor Cell::toColor() const
{
    if ( !m_busy )
    {
        return QColor( 70, 0, 0 );
    }

    return QColor( 0, 200, 0 );
}

QString Automaton::name() const
{
    return "life";
}

IAutomaton::StatesList Automaton::getDefaultStates() const
{
    StatesList l;

    State s1;
    s1.m_name = "void";
    s1.push_back( StateProperty( QString::fromUtf8( "busy" ), 0 ) );
    l.push_back( s1 );

    State s2;
    s2.m_name = "element";
    s2.push_back( StateProperty( QString::fromUtf8( "busy" ), 1 ) );
    l.push_back( s2 );

    return l;
}

// вычисляет следующее состояние автомата
void Automaton::next()
{
    // для каждой клетки вычисляем соседей
    StateMatrix tmp = m_default;

    for ( int i = 1; i < m_height - 1; ++i )
    {
        for ( int j = 1; j < m_width - 1; ++j )
        {
            int count = 0;

            // считаем соседей
            for ( int k = -1; k < 2; ++k )
            {
                for ( int l = -1; l < 2; ++l )
                {
                    if ( k != 0 || l != 0 )
                    {
                        count += m_matrix[ i + k][ j + l ].m_busy;
                    }
                }
            }

            int was = m_matrix[i][j].m_busy;

            if ( (was == 0 && count == 3) || (was == 1 && count == 2) || (was == 1 && count == 3) )
            {
                tmp[i][j].m_busy = 1;
            }
        }
    }

    m_matrix = tmp;
}

void Automaton::store( const QString& name )
{
    Packer pak( Packer::own_buffer );

    // тип автомата - ширина - высота
    pak << unsigned( 0 ) << unsigned( m_width ) << unsigned( m_height );

    for ( int i = 0; i < m_width; ++i )
    {
        for ( int j = 0; j < m_height; ++j )
        {
            pak << uchar( m_matrix[i][j].m_busy );
        }
    }

    pak.buffer().store( name );
}

void Automaton::restore( const QString& name )
{
    DynBuffer buffer;
    buffer.restore( name );

    Extractor ex( buffer );

    unsigned automat_type, width, height;
    ex >> automat_type >> width >> height;

    if ( automat_type != 0 )
    {
        QMessageBox::warning( 0, QString::fromUtf8("Внимание!"), QString::fromUtf8("Автомат другого типа!") );

        return;
    }

    if ( width > 10000 || height > 10000 )
    {
        QMessageBox::warning( 0, QString::fromUtf8("Внимание!"), QString::fromUtf8("Нарушение формата файла!") );

        return;
    }

    init_clear( width, height );

    for ( int i = 0; i < m_width; ++i )
    {
        for ( int j = 0; j < m_height; ++j )
        {
            uchar busy;
            ex >> busy;

            m_matrix[i][j].m_busy = busy;
        }
    }
}


}











































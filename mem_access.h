#ifndef MEMACCESS_H
#define MEMACCESS_H

#include <vector>

#include "mem_canvas.h"

// добавление данных
class Packer
{
public:
	enum EType
	{
		own_buffer
	};

public:
	Packer();
	Packer( Buffer& buffer );
	Packer( EType type );

	virtual ~Packer();

	void attach( Buffer& buffer );

	void cometoPos( unsigned pos );

	void push( const void* pData, unsigned bytes_count );
	void push( const Buffer& buffer );

	template < class value_t >
		Packer& operator<< ( const value_t& val )
	{
		push( (const uchar*)&val, sizeof(value_t) );
		return *this;
	}

	const Buffer& buffer() const;
	Buffer& buffer();

	void clear();
	const uchar* data() const;

private:
	Buffer*		m_pBuffer;
	unsigned	m_pos;
	bool		m_isSelfBuffer;
};


// распаковка данных
class Extractor
{
public:
	Extractor(): m_pBuffer( NULL ) {}
	Extractor( Buffer& buffer );

	void attach( Buffer& buffer );

	const Buffer& buffer() const;
	Buffer& buffer();

	void cometoPos( unsigned pos );

	void get( uchar* pAddr, unsigned size );

	template < class val_t >
		Extractor& operator>> ( val_t& val )
	{
		get( (uchar*)&val, sizeof(val_t) );
		return *this;
	}

	uchar* pointer();
	void incrementPointer( unsigned delta );

private:
	Buffer*		m_pBuffer;

	uchar*		m_pPointer;
};

#endif





















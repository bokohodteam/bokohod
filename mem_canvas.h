#ifndef MEM_CANVAS_H
#define MEM_CANVAS_H

#include <vector>
#include <list>
#include <qstring.h>
#include <iostream>
#include <stdio.h>

typedef unsigned char	uchar;
typedef unsigned short	ushort;
typedef unsigned int	uint;
typedef unsigned long	ulong;

//	memory spaces

// обертка для существующего куска памяти
class MemWrapper
{
public:
	MemWrapper(): m_pBegin( NULL ), m_sizeBytes( 0 ) {}
	MemWrapper( void* pBegin, unsigned bytes_count ): m_pBegin( (uchar*)pBegin ), m_sizeBytes( bytes_count ) {}

	void set( void* pBegin, unsigned bytes_count )
	{
		m_pBegin = (uchar*)pBegin;
		m_sizeBytes = bytes_count;
	}

	uchar* data();
	const uchar* data() const;

	unsigned size() const;

	void resize( unsigned new_bytes_count );

private:
	uchar*		m_pBegin;
	unsigned	m_sizeBytes;
};

// динамический участок памяти
class MemDynamic
{
	typedef std::vector<uchar>	buffer_t;

public:
	MemDynamic()
	{
	}

	uchar* data();
	const uchar* data() const;
	unsigned size() const;

	void resize( unsigned new_bytes_count );

private:
	buffer_t	m_buffer;
};

class Buffer
{
public:
	virtual ~Buffer() {}

	virtual bool store( const QString& fname ) const = 0;
	virtual bool restore( const QString& fname ) = 0;

	virtual uchar* data() = 0;
	virtual const uchar* data() const = 0;
	virtual unsigned size() const = 0;

	virtual void resize( unsigned new_bytes_count ) = 0;

	virtual quint8& u8( unsigned index ) = 0;
	virtual quint16& u16( unsigned index ) = 0;
	virtual quint32& u32( unsigned index ) = 0;
	virtual quint64& u64( unsigned index ) = 0;
};

// функциональность буфера
template < class mem_t >
class BufferT: public Buffer, public mem_t
{
public:
	uchar* data()
	{
		return mem_t::data();
	}

	const uchar* data() const
	{
		return mem_t::data();
	}

	unsigned size() const
	{
		return mem_t::size();
	}

	bool store( const QString& fname ) const
	{
		FILE* p = fopen( fname.toAscii(), "wb" );
		if ( !p )
		{
			return false;
		}

		fwrite( data(), size(), 1, p );
		fclose( p );

		return true;
	}

	bool restore( const QString& fname )
	{
		FILE* p = fopen( fname.toAscii(), "rb" );
		if ( !p )
		{
			return false;
		}

		fseek( p, 0, SEEK_END );
		unsigned fsize = ftell( p );
		fseek( p, 0, SEEK_SET );

		resize( fsize );

		fread( data(), size(), 1, p );
		fclose( p );

		return true;
	}

	void resize( unsigned new_bytes_count )
	{
		mem_t::resize( new_bytes_count );
	}

	quint8& u8( unsigned index )
	{
		return ( (quint8*)data() )[index];
	}

	quint16& u16( unsigned index )
	{
		return ( (quint16*)data() )[index];
	}

	quint32& u32( unsigned index )
	{
		return ( (quint32*)data() )[index];
	}

	quint64& u64( unsigned index )
	{
		return ( (quint64*)data() )[index];
	}
};

typedef BufferT<MemDynamic>		DynBuffer;
typedef BufferT<MemWrapper>		WrapBuffer;


#endif

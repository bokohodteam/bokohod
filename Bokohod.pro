#-------------------------------------------------
#
# Project created by QtCreator 2015-07-24T00:11:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Bokohod
TEMPLATE = app

CONFIG += console


SOURCES += main.cpp\
        mainwindow.cpp \
    testingarea.cpp \
    life.cpp \
    mem_access.cpp \
    mem_canvas.cpp

HEADERS  += mainwindow.h \
    testingarea.h \
    iautomaton.h \
    life.h \
    mem_access.h \
    mem_canvas.h

FORMS    += mainwindow.ui

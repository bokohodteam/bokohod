#ifndef TESTINGAREA_H
#define TESTINGAREA_H

#include <QBrush>
#include <QPainter>
#include <QRect>
#include <QPixmap>
#include <QWidget>
#include <QScrollBar>

#include "iautomaton.h"

class TestingArea: public QWidget
{
    Q_OBJECT

    enum EMouseMode
    {
        viewing,
        rbutton_drag
    };

public:
    TestingArea( QWidget* parent = 0 );

    void setScrolls( QScrollBar* pHor, QScrollBar* pVert );

    void attach( IAutomaton* p );
    void init();

    void setModeEditing( const State& state, bool is_editing );

protected:
    void paintEvent(QPaintEvent *);

private:
    void draw( QPainter& painter );

    void draw_grid( QPainter& painter );
    void draw_automaton( QPainter& painter );

    QRect cell_rect( int row, int col ) const;

    void resizeEvent( QResizeEvent* event );
    void wheelEvent( QWheelEvent* event );
    void mouseMoveEvent( QMouseEvent* pEvent );
    void mousePressEvent( QMouseEvent* event );
    void mouseReleaseEvent( QMouseEvent* event );

    void processMouseMoving( QMouseEvent* event );
    void dragScreen();

    void changeMouseMode( EMouseMode mode );

private slots:
    void onHorScroll( int pos );
    void onVertScroll( int pos );

private:
    QBrush          m_background;
    IAutomaton*     m_pAutomaton;

    QScrollBar*     m_pHorScroll;
    QScrollBar*     m_pVertScroll;

    // рассчетная сторона
    int             m_baseSide;

    // масштабированная сторона
    int             m_side;

    // масштаб, x раз
    float           m_scale;

    int             m_screenWidth;
    int             m_screenHeight;

    int             m_horShift;
    int             m_vertShift;

    int             m_highlightedRow;
    int             m_highlightedCol;

    QPoint          m_lastMousePoint;
    QPoint          m_pressPlace;

    int             m_horShiftPlace;
    int             m_vertShiftPlace;

    int             m_leftMargin;
    int             m_topMargin;

    EMouseMode      m_mouseMode;

    bool            m_isEditing;
    State           m_stateToInsert;

    static const int m_sMargin;
    static const int m_sMinForLine;
};

#endif // TESTINGAREA_H

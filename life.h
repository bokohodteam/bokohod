#ifndef AUTOMATON_H
#define AUTOMATON_H

#include "iautomaton.h"

#include <QColor>

#include <vector>

namespace life
{

struct Cell
{
    Cell(): m_busy( 0 ) {}

    QColor toColor() const;

public:
    int     m_busy;
};

class Automaton: public IAutomaton
{
    typedef std::vector<Cell>       StateLine;
    typedef std::vector<StateLine>  StateMatrix;

public:
    Automaton();

    QString name() const;

    // parameters
    int width() const;
    int height() const;

    StatesList getDefaultStates() const;

    // initing
    void init_clear( int width, int height );

    // access
    QColor cellColor( int row, int col ) const;
    State cell( int row, int col ) const;

    void setCell( int row, int col, const State& state );

    // вычисляет следующее состояние автомата
    void next();

    void store( const QString& name );
    void restore( const QString& name );

private:
    StateMatrix     m_matrix;
    StateMatrix     m_default;
    int             m_width;
    int             m_height;
};

}

#endif // AUTOMATON_H
